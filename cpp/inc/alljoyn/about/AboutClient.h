/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

#ifndef ABOUTCLIENT_H_
#define ABOUTCLIENT_H_

#include <map>
#include <vector>
#include <string>
#include <queue>

#include <alljoyn/Message.h>
#include <alljoyn/BusListener.h>
#include <alljoyn/BusObject.h>
#include <alljoyn/BusAttachment.h>
#include <alljoyn/MsgArg.h>
#include <qcc/String.h>

namespace ajn {
namespace services {

/**
 * AboutClient is a helper class used by an AllJoyn IoE client application to discover services
 * being offered by nearby AllJoyn IoE service applications.
 * AboutClient enables the user of the class to interact with the remote AboutService instance exposing  the following methods:
 *  GetObjectDescriptions
 *  GetAboutData
 *  GetVersion
 *
 */
class AboutClient : public ajn::MessageReceiver {
  public:
    /**
     *	map of AboutData using qcc::String as key and ajn::MsgArg as value.
     */
    typedef std::map<qcc::String, ajn::MsgArg> AboutData;

    /**
     * map of ObjectDescriptions using qcc::String as key std::vector<qcc::String>   as value, describing interfaces
     *
     */
    typedef std::map<qcc::String, std::vector<qcc::String> > ObjectDescriptions;

    /**
     * AnnounceListner is used to register a callback with an AboutClient instance.
     * The callback is called whenever the AboutClient receives an Announce signal from a remote
     * AboutService instance.
     */
    class AnnounceListener {
      public:
        /**
         * Destruct AnnounceListener
         */
        virtual ~AnnounceListener() {
        }

        /**
         *  Called by AboutClient when an org.alljoyn.About.Announce sessionless signal is received.
         * @param[in] version of the AboutService
         * @param[in] port	used by the AboutService
         * @param[in] busName	is the well known name of the AboutService
         * @param[in] objectDescs is a reference to ObjectDescriptions  std::map<qcc::String, std::vector<qcc::String> >
         * @param[in] aboutData is a reference to AboutData std::map<qcc::String, ajn::MsgArg>
         */
        virtual void Announce(int version, int port, const char* busName, const ObjectDescriptions& objectDescs, const AboutData& aboutData) = 0;
    };

    /**
     * Construct an AboutClient.
     */
    AboutClient(ajn::BusAttachment& bus);

    /**
     * Destruct AboutClient
     */
    ~AboutClient();

    /**
     * Set the listener callback that AboutClient will call whenever a
     * new org.alljoyn.About.Announce message is received.
     * @param listener
     */
    void RegisterAnnounceListener(AnnounceListener& listener);

    /**
     * Get the ObjectDescription array for specified bus name.
     *
     * @param[in] busName Unique or well-known name of AllJoyn node to retrieve ObjectDescriptions from.
     * @param[in,out] objectDescs  objectDescs  Description of busName's remote objects.
     * @param[in] sessionId the session received  after joining AllJoyn session
     * @return ER_OK if successful.
     */
    QStatus GetObjectDescriptions(const char* busName, ObjectDescriptions& objectDescs, ajn::SessionId sessionId = 0);

    /**
     * Get the AboutData  for specified bus name.
     * @param[in] busName busName Unique or well-known name of AllJoyn node to retrieve ObjectDescriptions from.
     * @param[in] languageTag is the language used to request the AboutData.
     * @param[in,out] data is reference of AboutData that is filled by the function
     * @param[in] sessionId the session received  after joining AllJoyn session
     * @return ER_OK if successful.
     */
    QStatus GetAboutData(const char* busName, const char* languageTag, AboutData& data, ajn::SessionId sessionId = 0);

    /**
     *
     * @param[in] busName Unique or well-known name of AllJoyn node to retrieve About data from.
     * @param[in] version of the service.
     * @param[in] sessionId the session received  after joining AllJoyn session
     * @return ER_OK if successful.
     */
    QStatus GetVersion(const char* busName, int& version, ajn::SessionId sessionId = 0);

  private:

    /**
     * AnnounceHandler is a callback registered to receive AllJoyn Signal.
     * @param[in] member
     * @param[in] srcPath
     * @param[in] message
     */
    void AnnounceHandler(const ajn::InterfaceDescription::Member* member, const char* srcPath, ajn::Message& message);

    /**
     *	pointer to  BusAttachment
     */
    ajn::BusAttachment* myBusAttachment;

    /**
     * pointer to  AnnounceListener
     */
    AnnounceListener* announceListener;
    /**
     * pointer to  ajn::InterfaceDescription::Member
     */
    const ajn::InterfaceDescription::Member* announceSignalMember;

    /**
     *	Thread identifier
     */
    pthread_t responderThread;

    /**
     * Queue of  pairs  ajn::Message, time_t
     */
    std::queue<std::pair<ajn::Message, time_t> > messagesQueue;

    /**
     *	 used to lock the messagesQueue
     */
    pthread_mutex_t lock;

    /**
     * used to signal changes to the messagesQueue
     */
    pthread_cond_t queueChanged;

    /**
     *	holds stopping status of the thread.
     */
    bool stopping;

    /**
     *
     * @param arg is void pointer to pass to the AboutClient
     */
    static void* ResponderThreadWrapper(void* arg);

    /**
     * called by ResponderThreadWrapper
     */
    void ResponderThread();

};

}
}

#endif /* ABOUTCLIENT_H_ */
