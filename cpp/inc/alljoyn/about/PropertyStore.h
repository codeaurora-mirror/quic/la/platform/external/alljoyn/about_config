/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/
#ifndef _PROPERTYSTORE_H
#define _PROPERTYSTORE_H



#include <alljoyn/Status.h>
#include <alljoyn/MsgArg.h>

namespace ajn {
namespace services {

#define ER_LANGUAGE_NOT_SUPPORTED       ((QStatus)0xb001)
#define ER_FEATURE_NOT_AVAILABLE        ((QStatus)0xb002)
#define ER_INVALID_VALUE                        ((QStatus)0xb003)
#define ER_MAX_SIZE_EXCEEDED            ((QStatus)0xb004)


/**
 * PropertyStore is implemented by the application , it is responsible to store the properties of the AboutServie and ConfigService.
 */
class PropertyStore {
  public:


    /**
     *
     */
    typedef enum {
        ANNOUNCE, //!< ANNOUNCE Property that has  ANNOUNCE  enabled
        READ,    //!< READ     Property that has READ  enabled
        WRITE,   //!< WRITE    Property that has  WRITE  enabled
    } Filter;


    /**
     * Calls Reset() implemented only for  ConfigService
     * @return status
     */
    virtual QStatus Reset() { return ER_NOT_IMPLEMENTED; }
    /**
     *
     * @param[in]languageTag is the language to use for the action can be NULL meaning default.
     * @param[in]filter describe which properties to read.
     * @param[out] all reference to MsgArg
     * @return status
     */
    virtual QStatus ReadAll(const char* languageTag, Filter filter, ajn::MsgArg& all) = 0;
    /**
     *
     * @param[in] name of the property
     * @param[in] languageTag is the language to use for the action can be NULL meaning default.
     * @param[in] value is a pointer to the data to change.
     * @return status
     */
    virtual QStatus Update(const char* name, const char* languageTag, const ajn::MsgArg* value) { return ER_NOT_IMPLEMENTED; }
    /**
     *
     * @param[in] name of the property
     * @param[in] languageTag is the language to use for the action can't be NULL.
     * @return[in] status
     */
    virtual QStatus Delete(const char* name, const char* languageTag) { return ER_NOT_IMPLEMENTED; }
    /**
     *  Desctructor of PropertyStore;
     */
    virtual ~PropertyStore() = 0;
};
inline PropertyStore::~PropertyStore() {
}

}
}

#endif /* _PROPERTYSTORE_H */
