/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

#include <qcc/platform.h>
#include <stdio.h>
#include <qcc/String.h>
#include <signal.h>
#include <alljoyn/version.h>


#include <qcc/Log.h>

#include <alljoyn/BusObject.h>
#include <alljoyn/BusAttachment.h>
#include <alljoyn/Status.h>

#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/tree.h>
#include <fstream>
#include <qcc/StringUtil.h>


#include <alljoyn/about/AboutIconService.h>
#include <alljoyn/about/AboutService.h>
#include <alljoyn/about/PropertyStore.h>




using namespace std;
using namespace ajn;

using namespace services;




static SessionPort SERVICE_PORT;




static volatile sig_atomic_t s_interrupt = false;

static void SigIntHandler(int sig)
{
    s_interrupt = true;
}




class MyBusListener : public BusListener, public SessionPortListener {
    void NameOwnerChanged(const char* busName, const char* previousOwner, const char* newOwner)
    {

    }
    bool AcceptSessionJoiner(SessionPort sessionPort, const char* joiner, const SessionOpts& opts)
    {
        if (sessionPort != SERVICE_PORT) {
            printf("Rejecting join attempt on unexpected session port %d.\n", sessionPort);
            return false;
        }
        printf("Accepting join session request from %s (opts.proximity=%x, opts.traffic=%x, opts.transports=%x).\n",
               joiner, opts.proximity, opts.traffic, opts.transports);
        return true;
    }
};


/** The bus listener object. */
static MyBusListener s_busListener;

/** Top level message bus object. */
static BusAttachment* s_msgBus = NULL;




/** Register the bus object and connect, report the result to stdout, and return the status code. */
QStatus RegisterBusObject(AboutService* obj)
{
    QStatus status = s_msgBus->RegisterBusObject(*obj);

    if (ER_OK == status) {
        printf("RegisterBusObject succeeded.\n");
    } else {
        printf("RegisterBusObject failed (%s).\n", QCC_StatusText(status));
    }

    return status;
}

/** Connect to the daemon, report the result to stdout, and return the status code. */
QStatus ConnectToDaemon(const char* connectSpec = NULL)
{
    QStatus status;
    if (connectSpec) {
        status = s_msgBus->Connect(connectSpec);
    } else {
        status = s_msgBus->Connect();
    }

    if (ER_OK == status) {
        printf("Connect to '%s' succeeded.\n", s_msgBus->GetConnectSpec().c_str());
    } else {
        printf("Failed to connect to '%s' (%s).\n", s_msgBus->GetConnectSpec().c_str(), QCC_StatusText(status));
    }

    return status;
}

/** Start the message bus, report the result to stdout, and return the status code. */
QStatus StartMessageBus(void)
{
    QStatus status = s_msgBus->Start();

    if (ER_OK == status) {
        printf("BusAttachment started.\n");
    } else {
        printf("Start of BusAttachment failed (%s).\n", QCC_StatusText(status));
    }

    return status;
}

/** Create the session, report the result to stdout, and return the status code. */
QStatus CreateSession(TransportMask mask)
{
    SessionOpts opts(SessionOpts::TRAFFIC_MESSAGES, false, SessionOpts::PROXIMITY_ANY, mask);
    SessionPort sp = SERVICE_PORT;
    QStatus status = s_msgBus->BindSessionPort(sp, opts, s_busListener);

    if (ER_OK == status) {
        printf("BindSessionPort succeeded.\n");
    } else {
        printf("BindSessionPort failed (%s).\n", QCC_StatusText(status));
    }

    return status;
}

/** Advertise the service name, report the result to stdout, and return the status code. */
QStatus AdvertiseName(TransportMask mask)
{
    QStatus status = ER_BUS_ESTABLISH_FAILED;
    if (s_msgBus->IsConnected() && s_msgBus->GetUniqueName().size() > 0) {
        status = s_msgBus->AdvertiseName(s_msgBus->GetUniqueName().c_str(), mask);
        printf("AdvertiseName %s =%d\n", s_msgBus->GetUniqueName().c_str(), status);
    }
    return status;
}





void WaitForSigInt(void)
{
    while (s_interrupt == false) {
#ifdef _WIN32
        Sleep(100);
#else
        usleep(100 * 1000);
#endif
    }
}

#define SERVICE_EXIT_OK       0
#define SERVICE_OPTION_ERROR  1
#define SERVICE_CONFIG_ERROR  2





static const char versionPreamble[] =
    "AboutService version: %s\n"
    "Copyright (c) 2009-2013 Qualcomm Innovation Center, Inc.\n"
    "Licensed under the 3-clause BSD license: http://opensource.org/licenses/BSD-3-Clause\n";
//"Build: %s\n";






class PropertyStoreImpl : public PropertyStore {

  public:
    class Property {
      public:
        Property(qcc::String aKeyName) : keyname(aKeyName), bPublic(false), bWritable(false), bAnnouncable(false) { }

        Property(qcc::String aKeyName, ajn::MsgArg aValue) : keyname(aKeyName), value(aValue), bPublic(false), bWritable(false), bAnnouncable(false) { }

        Property(qcc::String aKeyName, ajn::MsgArg aValue, bool isPublic, bool isWritable, bool isAnnouncable) : keyname(aKeyName), value(aValue), bPublic(isPublic), bWritable(isWritable), bAnnouncable(isAnnouncable) { }

        void SetFlags(bool isPublic, bool isWritable, bool isAnnouncable) { bPublic = isPublic; bWritable = isWritable; bAnnouncable = isAnnouncable; }
        void SetLanguage(qcc::String aLanguage) { language = aLanguage; }

        qcc::String& GetkeyName() { return keyname; }
        const ajn::MsgArg& GetkeyValue() { return value; }
        qcc::String& GetLanguage() { return language; }

        bool GetIsPublic() { return bPublic; }
        bool GetIsWritable() { return bWritable; }
        bool GetIsAnnouncable() { return bAnnouncable; }

        void SetIsPublic(bool value) { bPublic = value; }
        void SetIsWritable(bool value) { bWritable = value; }
        void SetIsAnnouncable(bool value) { bAnnouncable = value; }

      private:
        qcc::String keyname;
        ajn::MsgArg value;
        bool bPublic;
        bool bWritable;
        bool bAnnouncable;
        qcc::String language;
    };





  public:

    PropertyStoreImpl(std::multimap<qcc::String, Property>& data)
    {
        internalMultimap = data;
        argsAnnounceData = NULL;
        argsReadData = NULL;

    }

    virtual ~PropertyStoreImpl()
    {
        if (argsReadData != NULL) {
            delete argsReadData;
            argsReadData = NULL;
        }

        if (argsAnnounceData != NULL) {
            delete argsAnnounceData;
            argsAnnounceData = NULL;
        }

    }



    virtual QStatus ReadAll(const char* languageTag, Filter filter, ajn::MsgArg& all)  {

        QStatus status = ER_OK;
        if (filter == ANNOUNCE) {

            argsAnnounceData = new MsgArg[internalMultimap.size()];

            std::multimap<qcc::String, Property>::iterator search = internalMultimap.find("DefaultLanguage");

            qcc::String defaultLanguage;
            if (search != internalMultimap.end()) {
                char*temp;
                search->second.GetkeyValue().Get("s", &temp);
                defaultLanguage.assign(temp);
            }


            int announceArgCount = 0;
            for (std::multimap<qcc::String, Property>::const_iterator it = internalMultimap.begin(); it != internalMultimap.end(); ++it) {
                qcc::String key = it->first;
                Property property = it->second;
                if (!property.GetIsAnnouncable())
                    continue;
                // check that it is from the defaultLanguage or empty.
                if (!(property.GetLanguage().empty() || property.GetLanguage().compare(defaultLanguage) == 0))
                    continue;


                status = argsAnnounceData[announceArgCount].Set("{sv}", property.GetkeyName().c_str(), new MsgArg(property.GetkeyValue()));
                argsAnnounceData[announceArgCount].SetOwnershipFlags(MsgArg::OwnsArgs, true);
                announceArgCount++;
            }
            status = all.Set("a{sv}", announceArgCount, argsAnnounceData);
            all.SetOwnershipFlags(MsgArg::OwnsArgs, true);
            //printf("ReadAll ANNOUNCE status %d\n",status);

        } else if (filter == READ) {

            if (languageTag != NULL) {                                     // check that the language is in the supported languages;
                std::multimap<qcc::String, Property>::iterator it = internalMultimap.find("SupportedLanguages");
                if (it == internalMultimap.end())
                    return ER_LANGUAGE_NOT_SUPPORTED;

                const MsgArg*stringArray;
                size_t fieldListNumElements;
                it->second.GetkeyValue().Get("as", &fieldListNumElements, &stringArray);
                bool found = false;
                for (size_t i = 0; i < fieldListNumElements; i++) {
                    char* tempString;
                    stringArray[i].Get("s", &tempString);
                    if (strcmp(tempString, languageTag) == 0) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    return ER_LANGUAGE_NOT_SUPPORTED;
                }
            } else {
                return ER_LANGUAGE_NOT_SUPPORTED;
            }


            argsReadData = new MsgArg[internalMultimap.size()];
            int readArgCount = 0;



            for (std::multimap<qcc::String, Property>::const_iterator it = internalMultimap.begin(); it != internalMultimap.end(); ++it) {
                qcc::String key = it->first;
                Property property = it->second;
                if (!property.GetIsPublic())
                    continue;
                // check that it is from the defaultLanguage or empty.
                if (!(property.GetLanguage().empty() || property.GetLanguage().compare(languageTag) == 0))
                    continue;

                status = argsReadData[readArgCount].Set("{sv}", property.GetkeyName().c_str(), new  MsgArg(property.GetkeyValue()));
                argsReadData[readArgCount].SetOwnershipFlags(MsgArg::OwnsArgs, true);
                readArgCount++;
            }
            status = all.Set("a{sv}", readArgCount, argsReadData);
            all.SetOwnershipFlags(MsgArg::OwnsArgs, true);
            //printf("ReadAll READ status %d\n",status);
        } else {
            return ER_NOT_IMPLEMENTED;
        }
        return ER_OK;
        //printf("all.ToString() : %s\n\n", all.ToString().c_str());
    }
    virtual QStatus Update(const char* name, const char* languageTag, const ajn::MsgArg* value) { return ER_NOT_IMPLEMENTED; }
    virtual QStatus Delete(const char* name, const char* languageTag) { return ER_NOT_IMPLEMENTED; }

  private:
    MsgArg*argsAnnounceData;
    MsgArg*argsReadData;

    std::multimap<qcc::String, Property> internalMultimap;
};




class OptParse {
  public:
    enum ParseResultCode {
        PR_OK,
        PR_EXIT_NO_ERROR,
        PR_OPTION_CONFLICT,
        PR_INVALID_OPTION,
        PR_MISSING_OPTION,
        PR_INVALID_APPID

    };

    OptParse(int argc, char** argv) : argc(argc), argv(argv), internal(false) {
        port = 1000;
        appGUID.assign("000102030405060708090A0B0C0D0E0F");
    }

    ParseResultCode ParseResult();

    qcc::String GetConfigFile() const {
        return configFile;
    }

    qcc::String  GetDaemonSpec() const {
        return daemonSpec;
    }
    int  GetPort() const {
        return port;
    }
    qcc::String  GetAppID() const {
        return appGUID;
    }



    bool  GetInternal() const {
        return internal;
    }

  private:
    int argc;
    char** argv;


    bool internal;
    void PrintUsage();
    bool IsAllHex(const char* data);
    int ParseXML(const char* xmlData, std::multimap<qcc::String, PropertyStoreImpl::Property>& data);
  public:

    int ParseExternalXML(std::multimap<qcc::String, PropertyStoreImpl::Property>& data);
  private:
    qcc::String configFile;
    qcc::String daemonSpec;
    qcc::String appGUID;
    int port;
};


int OptParse::ParseExternalXML(std::multimap<qcc::String, PropertyStoreImpl::Property>& data)
{
    std::ifstream ifs(GetConfigFile().c_str());
    std::string content((std::istreambuf_iterator<char>(ifs)),
                        (std::istreambuf_iterator<char>()));

    if (content.empty()) {
        fprintf(stderr, "Unable to read XML file %s\n", GetConfigFile().c_str());
        return 1;
    }
    return ParseXML(content.c_str(), data);
}


int OptParse::ParseXML(const char* xmlData, std::multimap<qcc::String, PropertyStoreImpl::Property>& data)
{
    LIBXML_TEST_VERSION

    xmlParserCtxtPtr ctxt;             /* the parser context */
    xmlDocPtr doc;             /* the resulting document tree */
    xmlNode*root_element = NULL;
    int ret = 0;

    /* create a parser context */
    ctxt = xmlNewParserCtxt();
    if (ctxt == NULL) {
        fprintf(stderr, "Failed to allocate parser context\n");
        return 1;
    }


    doc = xmlCtxtReadMemory(ctxt, xmlData, strlen(xmlData), NULL, NULL, XML_PARSE_NOERROR | XML_PARSE_NOBLANKS);

    /* check if parsing suceeded */
    if (doc == NULL) {
        fprintf(stderr, "Unable to read  XML document\n");
        ret = 1;
    } else {
        /* check if validation suceeded */
        if (ctxt->valid == 0) {
            fprintf(stderr, "Invalid XML\n");
            ret = 1;
        } else {
            root_element = xmlDocGetRootElement(doc);

            xmlNode* configChildren = root_element->children;
            xmlNode* currentKey = configChildren->children;

            while (currentKey != NULL) {

                //printf("%s\n",currentKey->name);
                if (currentKey->type == XML_ELEMENT_NODE && currentKey->children != NULL) {
                    xmlChar* keyName = xmlGetProp(currentKey, (const xmlChar*)"name");
                    xmlNode* currentKeyLanguage = currentKey->children;

                    while (currentKeyLanguage != NULL) {
                        if (currentKeyLanguage->type == XML_ELEMENT_NODE) {
                            xmlChar* languageName = xmlGetProp(currentKeyLanguage, (const xmlChar*)"language");
                            if (currentKeyLanguage->children != NULL && currentKeyLanguage->children->content != NULL) {
                                xmlChar* language_value = currentKeyLanguage->children->content;
                                if (xmlStrEqual(keyName, (const xmlChar*)"DefaultLanguage")) {
                                    PropertyStoreImpl::Property defaultLanguage("DefaultLanguage", MsgArg("s", (const char*)language_value), true, true, true);
                                    data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("DefaultLanguage", defaultLanguage));
                                }
                                if (xmlStrEqual(keyName, (const xmlChar*)"DeviceName")) {
                                    PropertyStoreImpl::Property deviceName("DeviceName", MsgArg("s", (const char*)language_value), true, true, true);
                                    if (languageName != NULL) {
                                        deviceName.SetLanguage((const char*)languageName);
                                    }
                                    data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("DeviceName", deviceName));
                                }
                                if (xmlStrEqual(keyName, (const xmlChar*)"DeviceId")) {
                                    PropertyStoreImpl::Property deviceId("DeviceId", MsgArg("s", (const char*)language_value),     true, false, true);
                                    data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("DeviceId", deviceId));
                                }
                                if (xmlStrEqual(keyName, (const xmlChar*)"Description")) {
                                    PropertyStoreImpl::Property description("Description", MsgArg("s", (const char*)language_value), true, false, false);
                                    if (languageName != NULL) {
                                        description.SetLanguage((const char*)languageName);
                                    }
                                    data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("Description", description));
                                }
                            }
                        }
                        currentKeyLanguage = currentKeyLanguage->next;
                    }
                }
                currentKey = currentKey->next;
            }

        }

    }


    xmlCleanupParser();
    xmlMemoryDump();
    return ret;
}



void OptParse::PrintUsage() {
    qcc::String cmd = argv[0];
    cmd = cmd.substr(cmd.find_last_of('/') + 1);

    fprintf(
        stderr,
        "%s [--port|  | --config-file=FILE |  --daemonspec | --appid"
        "]\n"
        "    --daemonspec=\n"
        "       daemon spec used by the service.\n\n"
        "    --port=\n"
        "        used to bind the service.\n\n"
        "    --config-file=FILE\n"
        "        Use the specified configuration file.\n\n"
        "    --appid=\n"
        "        Use the specified it is HexString of 16 bytes (32 chars) \n\n"
        "    --version\n"
        "        Print the version and copyright string, and exit.\n",
        cmd.c_str());
}



bool OptParse::IsAllHex(const char* data)
{


    for (size_t index = 0; index < strlen(data); ++index) {
        if (!isxdigit(data[index]))
            return false;

    }
    return true;

}

OptParse::ParseResultCode OptParse::ParseResult()
{
    ParseResultCode result = PR_OK;
    int i;

    if (argc == 1) {
        internal = true;
        goto exit;
    }

    for (i = 1; i < argc; ++i) {
        qcc::String arg(argv[i]);
        if (arg.compare("--version") == 0) {
            printf(versionPreamble, "1");
            result = PR_EXIT_NO_ERROR;
            goto exit;
        } else if (arg.compare(0, sizeof("--port") - 1, "--port") == 0) {
            port = atoi(arg.substr(sizeof("--port")).c_str());
        } else if (arg.compare(0, sizeof("--daemonspec") - 1, "--daemonspec") == 0) {
            daemonSpec = arg.substr(sizeof("--daemonspec"));
        } else if (arg.compare(0, sizeof("--appid") - 1, "--appid") == 0) {
            appGUID = arg.substr(sizeof("--appid"));
            if ((appGUID.length() != 32) || (!IsAllHex(appGUID.c_str()))) {
                result = PR_INVALID_APPID;
                goto exit;
            }


        } else if (arg.compare("--config-file") == 0) {
            if (!configFile.empty() || internal) {
                result = PR_OPTION_CONFLICT;
                goto exit;
            }
            ++i;
            if (i == argc) {
                result = PR_MISSING_OPTION;
                goto exit;
            }
            configFile = argv[i];
        } else if (arg.compare(0, sizeof("--config-file") - 1, "--config-file") == 0) {
            if (!configFile.empty() || internal) {
                result = PR_OPTION_CONFLICT;
                goto exit;
            }
            configFile = arg.substr(sizeof("--config-file"));
        }  else if ((arg.compare("--help") == 0) || (arg.compare("-h") == 0)) {
            PrintUsage();
            result = PR_EXIT_NO_ERROR;
            goto exit;
        } else {
            result = PR_INVALID_OPTION;
            goto exit;
        }
    }

exit:

    internal = configFile.empty();

    switch (result) {
    case PR_OPTION_CONFLICT:
        fprintf(stderr,
                "Option \"%s\" is in conflict with a previous option.\n",
                argv[i]);
        break;

    case PR_INVALID_APPID:
        fprintf(stderr, "Invalid appid: \"%s\"\n", argv[i]);
        break;

    case PR_INVALID_OPTION:
        fprintf(stderr, "Invalid option: \"%s\"\n", argv[i]);
        break;

    case PR_MISSING_OPTION:
        fprintf(stderr, "No config file specified.\n");
        PrintUsage();
        break;

    default:
        break;
    }
    return result;
}




int main(int argc, char**argv, char**envArg) {
    QStatus status = ER_OK;
    printf("AllJoyn Library version: %s\n", ajn::GetVersion());
    printf("AllJoyn Library build info: %s\n", ajn::GetBuildInfo());
    QCC_SetLogLevels("ALLJOYN_ABOUT_SERVICE=7;");
    QCC_SetLogLevels("ALLJOYN_ABOUT_ICON_SERVICE=7;");


    OptParse opts(argc, argv);
    OptParse::ParseResultCode parseCode(opts.ParseResult());
    switch (parseCode) {
    case OptParse::PR_OK:
        break;

    case OptParse::PR_EXIT_NO_ERROR:

        return SERVICE_EXIT_OK;

    default:
        return SERVICE_OPTION_ERROR;
    }

    SERVICE_PORT = opts.GetPort();

    if (!opts.GetDaemonSpec().empty()) {
        printf("using Daemon config spec %s\n", opts.GetDaemonSpec().c_str());
    }

    if (!opts.GetAppID().empty()) {
        printf("using appID %s\n", opts.GetAppID().c_str());
    }


    printf("using port %d\n", opts.GetPort());


    std::multimap<qcc::String, PropertyStoreImpl::Property> data;
    if (opts.GetConfigFile().empty()) {
        printf("Config-file is not used\n");
    } else {
        printf("using Config-file %s\n", opts.GetConfigFile().c_str());
        if (opts.ParseExternalXML(data) != 0)
            return 1;
    }



    /* Install SIGINT handler so Ctrl + C deallocates memory properly */
    signal(SIGINT, SigIntHandler);


    /* Create message bus */
    s_msgBus = new BusAttachment("myApp", true);

    if (!s_msgBus) {
        status = ER_OUT_OF_MEMORY;
        return status;
    }


    if (ER_OK == status) {
        status = StartMessageBus();
    }

    if (ER_OK == status) {

        if (opts.GetDaemonSpec().empty()) {
            status = ConnectToDaemon();
        } else {
            status = ConnectToDaemon(opts.GetDaemonSpec().c_str());
        }

    }

    if (ER_OK == status) {
        s_msgBus->RegisterBusListener(s_busListener);
    }


    AboutService*aboutService = NULL;
    AboutIconService*aboutIconService = NULL;
    PropertyStoreImpl* propertyStoreImpl = NULL;
    if (ER_OK == status) {

        if (data.size() == 0) {
            // public.write.announce
            data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("DefaultLanguage",  PropertyStoreImpl::Property("DefaultLanguage", MsgArg("s", "en"),     true, true, true)));


            PropertyStoreImpl::Property DeviceName1("DeviceName", MsgArg("s", "Device Name"), true, true, true); DeviceName1.SetLanguage("en");
            PropertyStoreImpl::Property DeviceName2("DeviceName", MsgArg("s", "Nom de l'appareil"), true, true, true); DeviceName2.SetLanguage("fr");
            data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("DeviceName", DeviceName1));
            data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("DeviceName", DeviceName2));


            data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("DeviceId",  PropertyStoreImpl::Property("DeviceId", MsgArg("s", "Device ID en"),         true, false, true)));




            PropertyStoreImpl::Property Description1("Description", MsgArg("s", "Run the app"),                               true, false, false); Description1.SetLanguage("en");
            PropertyStoreImpl::Property Description2("Description", MsgArg("s", "exécuter l'application"),   true, false, false); Description2.SetLanguage("fr");
            data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("Description", Description1));
            data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("Description", Description2));

            //data.insert(std::pair<qcc::String,PropertyStoreImpl::Property> ("Description",  PropertyStoreImpl::Property("Description",MsgArg("s","This is car"),               true,false,false)));
        }

        uint8_t AppId[16];
        qcc::HexStringToBytes(opts.GetAppID(), AppId, 16);
        data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("AppId",  PropertyStoreImpl::Property("AppId", MsgArg("ay", sizeof(AppId) / sizeof (*AppId), AppId),                                        true, false, true)));


        PropertyStoreImpl::Property AppName1("AppName", MsgArg("s", "App Name en"), true, false, true); AppName1.SetLanguage("en");
        PropertyStoreImpl::Property AppName2("AppName", MsgArg("s", "nom de l'application"), true, false, true); AppName2.SetLanguage("fr");
        data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("AppName", AppName1));
        data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("AppName", AppName2));

        PropertyStoreImpl::Property Manufacturer1("Manufacturer", MsgArg("s", "Company en"), true, false, true); Manufacturer1.SetLanguage("en");
        PropertyStoreImpl::Property Manufacturer2("Manufacturer", MsgArg("s", "Company fr"), true, false, true); Manufacturer2.SetLanguage("fr");
        data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("Manufacturer", Manufacturer1));
        data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("Manufacturer", Manufacturer2));


        data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("ModelNumber",  PropertyStoreImpl::Property("ModelNumber", MsgArg("s", "Model Number"),                   true, false, true)));
        // public.write.announce


        const char*languages [] = { "en", "fr" };
        data.insert(std::pair<qcc::String, PropertyStoreImpl::Property>
                        ("SupportedLanguages",  PropertyStoreImpl::Property("SupportedLanguages", MsgArg("as", sizeof(languages) / sizeof (*languages), languages), true, false, false)));

        //data.insert(std::pair<qcc::String,PropertyStoreImpl::Property> ("DateOfManufacture",  PropertyStoreImpl::Property("DateOfManufacture",MsgArg("s","2019-12-12"),               true,false,false)));
        data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("SoftwareVersion",  PropertyStoreImpl::Property("SoftwareVersion", MsgArg("s", "Software Version"),               true, false, false)));
        data.insert(std::pair<qcc::String, PropertyStoreImpl::Property> ("AJSoftwareVersion",  PropertyStoreImpl::Property("AJSoftwareVersion", MsgArg("s", ajn::GetVersion()),   true, false, false)));
        //data.insert(std::pair<qcc::String,PropertyStoreImpl::Property> ("HardwareVersion",  PropertyStoreImpl::Property("HardwareVersion",MsgArg("s","HardwareVersion"),		true,false,false)));
        //data.insert(std::pair<qcc::String,PropertyStoreImpl::Property> ("SupportedUrl",  PropertyStoreImpl::Property("SupportedUrl",MsgArg("s","http://www.alljoyn.org"),		true,false,false)));


        propertyStoreImpl = new PropertyStoreImpl(data);



        aboutService = new AboutService(*s_msgBus, *propertyStoreImpl);

        aboutService->Register(SERVICE_PORT);
        status = RegisterBusObject(aboutService);

        uint8_t aboutIconContent[] = { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A,
                                       0x00, 0x00, 0x00, 0x0D, 0x49, 0x48, 0x44, 0x52,
                                       0x00, 0x00, 0x00, 0x0A, 0x00, 0x00, 0x00, 0x0A,
                                       0x08, 0x02, 0x00, 0x00, 0x00, 0x02, 0x50, 0x58,
                                       0xEA, 0x00, 0x00, 0x00, 0x04, 0x67, 0x41, 0x4D,
                                       0x41, 0x00, 0x00, 0xAF, 0xC8, 0x37, 0x05, 0x8A,
                                       0xE9, 0x00, 0x00, 0x00, 0x19, 0x74, 0x45, 0x58,
                                       0x74, 0x53, 0x6F, 0x66, 0x74, 0x77, 0x61, 0x72,
                                       0x65, 0x00, 0x41, 0x64, 0x6F, 0x62, 0x65, 0x20,
                                       0x49, 0x6D, 0x61, 0x67, 0x65, 0x52, 0x65, 0x61,
                                       0x64, 0x79, 0x71, 0xC9, 0x65, 0x3C, 0x00, 0x00,
                                       0x00, 0x18, 0x49, 0x44, 0x41, 0x54, 0x78, 0xDA,
                                       0x62, 0xFC, 0x3F, 0x95, 0x9F, 0x01, 0x37, 0x60,
                                       0x62, 0xC0, 0x0B, 0x46, 0xAA, 0x34, 0x40, 0x80,
                                       0x01, 0x00, 0x06, 0x7C, 0x01, 0xB7, 0xED, 0x4B,
                                       0x53, 0x2C, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45,
                                       0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82 };


        qcc::String mimeType("image/png");
        qcc::String url("http://tinyurl.com/m3ra78j");


        std::vector<qcc::String> interfaces;
        interfaces.push_back("org.alljoyn.Icon");
        aboutService->AddObjectDescription("/About/DeviceIcon", interfaces);


        aboutIconService = new AboutIconService(*s_msgBus, mimeType, url, aboutIconContent, sizeof(aboutIconContent) / sizeof (*aboutIconContent));
        aboutIconService->Register();

        status = s_msgBus->RegisterBusObject(*aboutIconService);



    }



    const TransportMask SERVICE_TRANSPORT_TYPE = TRANSPORT_ANY;

    if (ER_OK == status) {
        status = CreateSession(SERVICE_TRANSPORT_TYPE);
    }


    if (ER_OK == status) {
        status = AdvertiseName(SERVICE_TRANSPORT_TYPE);
    }

    if (ER_OK == status) {
        status = aboutService->Announce();

    }


    /* Perform the service asynchronously until the user signals for an exit. */
    if (ER_OK == status) {
        WaitForSigInt();
    }


    /* Clean up msg bus */
    delete s_msgBus;
    s_msgBus = NULL;

    if (!aboutService) {
        delete aboutService;
        aboutService = NULL;
    }

    if (!aboutIconService) {
        delete aboutIconService;
        aboutIconService = NULL;
    }


    if (!propertyStoreImpl) {
        delete propertyStoreImpl;
        propertyStoreImpl = NULL;

    }



    return 0;
} /* main() */




