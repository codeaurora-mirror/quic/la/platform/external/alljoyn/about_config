/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

#include <stdint.h>
#include <algorithm>
#include <stdio.h>
#include <alljoyn/about/AboutService.h>
#include <alljoyn/about/PropertyStore.h>
#include <alljoyn/BusAttachment.h>
#include <alljoyn/BusObject.h>
#include <qcc/Debug.h>
#define QCC_MODULE "ALLJOYN_ABOUT_SERVICE"

using namespace ajn;
using namespace services;

static const char* about_interface_name = "org.alljoyn.About";

AboutService::AboutService(ajn::BusAttachment& bus, PropertyStore& store) :
    BusObject("/About"), announcePort(0) {

    QCC_DbgTrace(("AboutService::%s", __FUNCTION__));
    myBusAttachment = &bus;
    myPropertyStore = &store;
    announceSignalMember = NULL;
    std::vector<qcc::String> v;
    v.push_back(about_interface_name);
    controlInterfaceMap.insert(
        std::pair<qcc::String, std::vector<qcc::String> >("/About", v));

}

QStatus AboutService::Register(int port) {
    QCC_DbgTrace(("AboutService::%s", __FUNCTION__));
    QStatus status = ER_OK;

    announcePort = port;
    const InterfaceDescription* getIface = NULL;
    getIface = myBusAttachment->GetInterface(about_interface_name);
    if (!getIface) {
        InterfaceDescription* createIface = NULL;
        status = myBusAttachment->CreateInterface(about_interface_name,
                                                  createIface, false);
        status = createIface->AddMethod("GetAboutData", "s", "a{sv}",
                                        "languageTag,aboutData");
        status = createIface->AddMethod("GetObjectDescription", NULL, "a(oas)",
                                        "Control");
        status = createIface->AddSignal("Announce", "qqa(oas)a{sv}",
                                        "version,port,objectDescription,servMetadata");
        status = createIface->AddProperty("Version", "q",
                                          (uint8_t) PROP_ACCESS_READ);
        createIface->Activate();
        status = AddInterface(*createIface);
        status =
            AddMethodHandler(createIface->GetMember("GetAboutData"),
                             static_cast<MessageReceiver::MethodHandler>(&AboutService::GetAboutDataHandler));
        status =
            AddMethodHandler(createIface->GetMember("GetObjectDescription"),
                             static_cast<MessageReceiver::MethodHandler>(&AboutService::GetObjectDescriptionHandler));
        if (status == ER_OK) {
            announceSignalMember = createIface->GetMember("Announce");
            assert(announceSignalMember);
        }
    } else {
        status = AddInterface(*getIface);
        status =
            AddMethodHandler(getIface->GetMember("GetAboutData"),
                             static_cast<MessageReceiver::MethodHandler>(&AboutService::GetAboutDataHandler));
        status =
            AddMethodHandler(getIface->GetMember("GetObjectDescription"),
                             static_cast<MessageReceiver::MethodHandler>(&AboutService::GetObjectDescriptionHandler));
        if (status == ER_OK) {
            announceSignalMember = getIface->GetMember("Announce");
            assert(announceSignalMember);
        }
    }
    return status;
}

void AboutService::Unregister() {
    QCC_DbgTrace(("AboutService::%s", __FUNCTION__));
}

void AboutService::AddObjectDescription(const ajn::BusObject& obj) {
}

void AboutService::RemoveObjectDescription(const ajn::BusObject& obj) {

}

QStatus AboutService::AddObjectDescription(qcc::String path,
                                           std::vector<qcc::String> interfaceNames) {
    QCC_DbgTrace(("AboutService::%s", __FUNCTION__));
    QStatus status = ER_OK;
    std::map<qcc::String, std::vector<qcc::String> >::iterator it =
        controlInterfaceMap.find(path);
    if (it != controlInterfaceMap.end()) {
        interfaceNames.insert(interfaceNames.end(), it->second.begin(),
                              it->second.end());
        controlInterfaceMap.erase(it);
        controlInterfaceMap.insert(
            std::pair<qcc::String, std::vector<qcc::String> >(path,
                                                              interfaceNames));
    } else {
        controlInterfaceMap.insert(
            std::pair<qcc::String, std::vector<qcc::String> >(path,
                                                              interfaceNames));
    }
    return status;
}

QStatus AboutService::RemoveObjectDescription(qcc::String path,
                                              std::vector<qcc::String> interfaceNames) {
    QCC_DbgTrace(("AboutService::%s", __FUNCTION__));
    QStatus status = ER_OK;
    std::map<qcc::String, std::vector<qcc::String> >::iterator it =
        controlInterfaceMap.find(path);
    if (it != controlInterfaceMap.end()) {
        for (std::vector<qcc::String>::const_iterator itv =
                 interfaceNames.begin(); itv != interfaceNames.end(); ++itv) {
            std::vector<qcc::String>::iterator findIterator = std::find(
                it->second.begin(), it->second.end(), itv->c_str());
            if (findIterator != it->second.end()) {
                it->second.erase(findIterator);
            }
        }
    }
    return status;
}

QStatus AboutService::Announce() {
    QCC_DbgTrace(("AboutService::%s", __FUNCTION__));
    QStatus status = ER_OK;
    if (announceSignalMember == NULL) {
        return ER_FAIL;
    }
    MsgArg announceArgs[4];
    status = announceArgs[0].Set("q", 1);
    status = announceArgs[1].Set("q", announcePort);
    MsgArg outerArg[controlInterfaceMap.size()];
    int outerindex = 0;
    for (std::map<qcc::String, std::vector<qcc::String> >::const_iterator it =
             controlInterfaceMap.begin(); it != controlInterfaceMap.end();
         ++it) {
        qcc::String key = it->first;
        std::vector<qcc::String> internalVector = it->second;
        const char* interanlEntries[internalVector.size()];
        int innerIndex = 0;
        for (std::vector<qcc::String>::const_iterator innertIt =
                 internalVector.begin(); innertIt != internalVector.end();
             ++innertIt) {
            interanlEntries[innerIndex] = innertIt->c_str();
            innerIndex++;
        }
        status = outerArg[outerindex].Set("(oas)", key.c_str(), innerIndex,
                                          interanlEntries);
        outerindex++;
    }
    status = announceArgs[2].Set("a(oas)", outerindex, outerArg);
    myPropertyStore->ReadAll(NULL, PropertyStore::ANNOUNCE, announceArgs[3]);
    Message msg(*myBusAttachment);
    uint8_t flags = ALLJOYN_FLAG_SESSIONLESS;
#if !defined(NDEBUG)
    for (int i = 0; i < 4; i++) {
        QCC_DbgPrintf(
            ("announceArgs[%d]=%s", i, announceArgs[i].ToString().c_str()));
    }
#endif
    status = Signal(NULL, 0, *announceSignalMember, announceArgs, 4,
                    (unsigned char) 0, flags);
    QCC_DbgPrintf(
        ("EmitConnectionResultChangedSignal from %s  =%d", myBusAttachment->GetUniqueName().c_str(), status));
    return status;

}

void AboutService::GetAboutDataHandler(
    const ajn::InterfaceDescription::Member* member, ajn::Message& msg) {
    QCC_DbgTrace(("AboutService::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const ajn::MsgArg* args;
    size_t numArgs;
    msg->GetArgs(numArgs, args);
    if (numArgs == 1) {
        ajn::MsgArg retargs[1];
        status = myPropertyStore->ReadAll(args[0].v_string.str,
                                          PropertyStore::READ, retargs[0]);
        QCC_DbgPrintf(
            ("myPropertyStore->ReadAll(%s,PropertyStore::READ)  =%s", args[0].v_string.str, QCC_StatusText(status)));
        if (status != ER_OK) {
            if (status == ER_LANGUAGE_NOT_SUPPORTED) {
                MethodReply(msg, "org.alljoyn.Error.LanguageNotSupported",
                            "The language specified is not supported");
                return;
            }
            MethodReply(msg, status);
            return;
        } else {
            status = MethodReply(msg, retargs, 1);
        }
    } else {
        MethodReply(msg, ER_INVALID_DATA);
    }
}

void AboutService::GetObjectDescriptionHandler(
    const ajn::InterfaceDescription::Member* member, ajn::Message& msg) {
    QCC_DbgTrace(("AboutService::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const ajn::MsgArg* args;
    size_t numArgs;
    msg->GetArgs(numArgs, args);
    if (numArgs == 0) {
        ajn::MsgArg retargs[1];
        MsgArg outerArg[controlInterfaceMap.size()];
        int outerindex = 0;
        for (std::map<qcc::String, std::vector<qcc::String> >::const_iterator it =
                 controlInterfaceMap.begin(); it != controlInterfaceMap.end();
             ++it) {
            qcc::String key = it->first;
            std::vector<qcc::String> internalVector = it->second;
            const char* interanlEntries[internalVector.size()];
            int innerIndex = 0;
            for (std::vector<qcc::String>::const_iterator innertIt =
                     internalVector.begin(); innertIt != internalVector.end();
                 ++innertIt) {
                interanlEntries[innerIndex] = innertIt->c_str();
                innerIndex++;
            }
            status = outerArg[outerindex].Set("(oas)", key.c_str(), innerIndex,
                                              interanlEntries);
            outerindex++;
        }
        status = retargs[0].Set("a(oas)", outerindex, outerArg);
        status = MethodReply(msg, retargs, 1);
    } else {
        MethodReply(msg, ER_INVALID_DATA);
    }

}

QStatus AboutService::Get(const char*ifcName, const char*propName,
                          MsgArg& val) {
    QCC_DbgTrace(("AboutService::%s", __FUNCTION__));
    QStatus status = ER_OK;
    if (0 == strcmp(ifcName, about_interface_name)) {
        if (0 == strcmp("Version", propName)) {
            val.Set("q", 1);
        }
    } else {
        status = ER_BUS_NO_SUCH_PROPERTY;
    }
    return status;
}
