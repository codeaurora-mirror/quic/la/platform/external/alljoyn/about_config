/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.about;

import java.util.List;

import org.alljoyn.services.common.BusObjectDescription;

public interface Announcer
{
	/**
	 * cause an Announcement to be emitted.
	 */
	public void announce();

	/**
	 * whether Announcements are emitted.
	 * @return true if Announcements are emitted.
	 */
	public boolean isAnnouncing();

	/**
	 * set whether Announcements are emitted.
	 * @param enable enable Announcement emitting.
	 */
	public void setAnnouncing(boolean enable);

	/**
	 * Any service who registered a BusObject in the common BusAttachment, should save it here so that we can announce it 
	 * and include it in the About contents.
	 * @param objectPath the object path that was used when calling BusAtatchment.registerBusObject.  example: "/About"
	 * @param interfaces the AllJoyn interfaces that this bus object implements. example: org.alljoyn.About, org.alljoyn.Config ...
	 */
	public void addBusObjectAnnouncements(List<BusObjectDescription> descriptions);
	public void removeBusObjectAnnouncements(List<BusObjectDescription> descriptions);

}
