/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common;

import java.util.List;

import org.alljoyn.bus.BusAttachment;
import org.alljoyn.services.common.utils.GenericLogger;

public abstract class ServiceCommonImpl implements ServiceCommon {

	/********* General *********/
	private BusAttachment m_bus;
	protected static short m_port;
	protected GenericLogger m_logger;
	
	protected String TAG = "";
	private boolean m_isClientRunning;
	private boolean m_isServerRunning;
	
	/**
	 * The daemon should advertise itself "quietly" (directly to the calling port)
	 * This is to reply directly to a TC looking for a daemon 
	 */
	protected static final String DAEMON_NAME_PREFIX= "org.alljoyn.BusNode.";
	/**
	 * The daemon should advertise itself "quietly" (directly to the calling port)
	 * This is to reply directly to a TC looking for a daemon 
	 */
	protected static final String DAEMON_QUIET_PREFIX= "quiet@";

	protected void setBus(BusAttachment bus) throws BusAlreadyExistException
	{
		if (m_bus == null || m_bus.getUniqueName().equals(bus.getUniqueName()))
			m_bus = bus;
		else
			throw new BusAlreadyExistException(""); 
	}
	
	protected BusAttachment getBus()
	{
		return m_bus; 
	}
	
	protected void startClient()
	{
		m_isClientRunning = true;
	}
	
	protected void startServer()
	{
		m_isServerRunning = true;
	}
	
	protected void stopClient()
	{
		m_isClientRunning = false;
		if (!m_isServerRunning)
		{
			m_bus = null;
		}
	}
	
	protected void stopServer()
	{
		m_isServerRunning = false;
		if (!m_isClientRunning)
		{
			m_bus = null;
		}
	}
	
	@Override
	public boolean isClientRunning()
	{
		return m_isClientRunning;
	}
	
	@Override
	public boolean isServerRunning()
	{
		return m_isServerRunning;
	}
	
	public ServiceCommonImpl() {
		
	}
	@Override
	public void setLogger(GenericLogger logger)
	{
		m_logger = logger;
	}

	/*@Override
	public void initBus(BusAttachment busAttachment) throws Exception {
		m_bus = busAttachment;
	}*/

	/*@Override
	public boolean isConnected()
	{
		return m_bus != null && m_bus.isConnected();
	}*/

/*	protected void startAJ(String appName) throws Exception {
		if (m_bus == null) 
		{
			All communication through AllJoyn begins with a BusAttachment.  
			m_bus = new BusAttachment(appName, BusAttachment.RemoteMessage.Receive);

			//setting the password for the daemon to allow thin clients to connect
			getLogger().debug(TAG, "Setting daemon password");
			Status pasStatus = PasswordManager.setCredentials("ALLJOYN_PIN_KEYX", "000000");

			if ( pasStatus != Status.OK ) {
				getLogger().error(TAG, "Failed to set password for daemon, Error: " + pasStatus);
			}

			Status status = m_bus.connect();
			//logStatus("BusAttachment.connect()", status);
			if (Status.OK == status)
			{
				//request the name	
				int flag = BusAttachment.ALLJOYN_REQUESTNAME_FLAG_DO_NOT_QUEUE;
				Status reqStatus = m_bus.requestName(DAEMON_NAME_PREFIX + "G" + UUID.randomUUID(), flag);
				if (reqStatus == Status.OK) {
					//advertise the name
					//advertise the name with a quite prefix for TC to find it
					Status adStatus = m_bus.advertiseName(DAEMON_QUIET_PREFIX + DAEMON_NAME_PREFIX, SessionOpts.TRANSPORT_ANY);
					if (adStatus != Status.OK){
						m_bus.releaseName(DAEMON_NAME_PREFIX);
						getLogger().warn(TAG, "failed to advertise daemon name " + DAEMON_NAME_PREFIX);
					}
					else{
						getLogger().debug(TAG, "Succefully advertised daemon name " + DAEMON_NAME_PREFIX);
					}
				}	
			}
		}
	}*/

/*	private String bindSession(final short port)
	{
		
		 * Create a new session listening on the contact port of the about/config service.
		 
		Mutable.ShortValue contactPort = new Mutable.ShortValue(port);

		SessionOpts sessionOpts = new SessionOpts();
		sessionOpts.traffic = SessionOpts.TRAFFIC_MESSAGES;
		sessionOpts.isMultipoint = true;
		sessionOpts.proximity = SessionOpts.PROXIMITY_ANY;
		sessionOpts.transports = SessionOpts.TRANSPORT_ANY;

		Status status = getBus().bindSessionPort(contactPort, sessionOpts, new SessionPortListener() {
			@Override
			public boolean acceptSessionJoiner(short sessionPort, String joiner, SessionOpts sessionOpts) {
				if (sessionPort == port) {
					return true;
				} else {
					return false;
				}
			}
			public void sessionJoined(short sessionPort, int id, String joiner){
				getLogger().info(TAG, String.format("SessionPortListener.sessionJoined(%d, %d, %s)", sessionPort, id, joiner));

			}
		});
		String logMessage = String.format("BusAttachment.bindSessionPort(%d, %s): %s",
				contactPort.value, sessionOpts.toString(),status);
		getLogger().debug(TAG, logMessage);
		if (status != Status.OK) {
			return "";
		}

		String serviceName = getBus().getUniqueName();

		status = getBus().advertiseName(serviceName, SessionOpts.TRANSPORT_ANY);
		getLogger().debug(TAG, String.format("BusAttachement.advertiseName(%s): %s", serviceName, status));

		return serviceName;
	}

*/
	public GenericLogger getLogger(){
		if (m_logger == null)
		{
			m_logger =  new DefaultGenericLogger();
		}
		return m_logger;
	}

	@Override
	public abstract List<BusObjectDescription> getBusObjectDescriptions();

}
