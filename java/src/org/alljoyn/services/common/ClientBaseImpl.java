/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common;

import org.alljoyn.about.transport.AboutTransport;
import org.alljoyn.bus.BusAttachment;
import org.alljoyn.bus.BusException;
import org.alljoyn.bus.Mutable;
import org.alljoyn.bus.ProxyBusObject;
import org.alljoyn.bus.SessionListener;
import org.alljoyn.bus.SessionOpts;
import org.alljoyn.bus.Status;
import org.alljoyn.services.common.storage.PropertyStore;


public abstract class ClientBaseImpl implements ClientBase
{
	public final static String TAG = ClientBaseImpl.class.getName();
	
	protected BusAttachment m_bus;
	protected String m_peerName;
	int m_sessionId;
	protected char[] m_pinCode; // default is null !!!
	protected ServiceAvailabilityListener m_serviceAvailabilityListener;

	protected short m_port;
	
	protected PropertyStore m_propertyStore;
	
	protected String m_objectPath;

	protected Class m_objectClass;
	protected boolean m_isConnected = false;
	
	public ClientBaseImpl(String peerName, 
			BusAttachment bus, 
			ServiceAvailabilityListener serviceAvailabilityListener,
			String objectPath,
			Class objectClass,
			short port)
	{
		this.m_peerName = peerName;
		this.m_bus = bus;
		this.m_port = port;
		this.m_objectPath = objectPath;
		this.m_objectClass = objectClass;
		
		m_serviceAvailabilityListener = serviceAvailabilityListener;
	}


	@Override
	public void initBus(BusAttachment busAttachment) throws Exception {
		m_bus = busAttachment;
	}

	@Override
	public short getVersion() throws BusException {
		ProxyBusObject proxyObj = getProxyObject();
		/* We make calls to the methods of the AllJoyn object through one of its interfaces. */
		AboutTransport aboutTransport =  proxyObj.getInterface(AboutTransport.class);
		return aboutTransport.getVersion();
	}
	
	protected ProxyBusObject getProxyObject() 
	{
		ProxyBusObject m_proxyBusObj = m_bus.getProxyBusObject(getPeerName(), 
				getObjectPath(),getSessionId(),
				new Class<?>[] {getObjectClass()});
		return m_proxyBusObj;
	}

	public String getPeerName()
	{
		return m_peerName;
	}

	public int getSessionId()
	{
		return m_sessionId;
	}

	public String getObjectPath()
	{
		return m_objectPath;
	}

	public Class getObjectClass()
	{
		return m_objectClass;
	}

	@Override
	public void disconnect() throws BusException
	{
		Status status = m_bus.leaveSession(getSessionId());
		if (status == Status.OK)
		{
			m_isConnected = false;
		}
	}

	@Override
	public void connect() throws BusException
	{
		SessionOpts sessionOpts = new SessionOpts();
		sessionOpts.traffic = SessionOpts.TRAFFIC_MESSAGES;
		sessionOpts.isMultipoint = false;
		sessionOpts.proximity = SessionOpts.PROXIMITY_ANY;
		sessionOpts.transports = SessionOpts.TRANSPORT_ANY;
		Mutable.IntegerValue sessionId = new Mutable.IntegerValue();
		//deviceName
		Status status = m_bus.joinSession(getPeerName(), m_port, sessionId, sessionOpts, new SessionListener()
		{
			@Override
			public void sessionLost(int sessionId) 
			{
				if (getSessionId() == sessionId && m_serviceAvailabilityListener != null)
				{
					connectionLost();
				}
			}

			@Override
			public void sessionMemberAdded(int sessionId, String uniqueName) 
			{
			}

			public void sessionMemberRemoved(int sessionId, String uniqueName) 
			{
			}

		});

		if (Status.OK != status)
		{
			this.m_isConnected = false;
			throw new BusException("Failed connect to device. Status=" + status.toString());
		}
		this.m_isConnected = true;
		this.m_sessionId = sessionId.value;
	}

	public void connectionLost()
	{
		m_isConnected = false;
		if (m_serviceAvailabilityListener != null)
		{
			m_serviceAvailabilityListener.connectionLost();
		}
	}
	
	public boolean isConnected(){
		return m_isConnected;
	}
	
}
