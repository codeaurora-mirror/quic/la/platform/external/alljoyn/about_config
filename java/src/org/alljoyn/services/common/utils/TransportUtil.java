/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.alljoyn.about.AboutKeys;
import org.alljoyn.bus.BusException;
import org.alljoyn.bus.Variant;

public class TransportUtil
{

	/**
	 * Utility method to convert from a String->Object map to a String-Variant map
	 * @param sToObjectMap input String->Object map.
	 * @return String->Variant map
	 */
	public static Map<String, Variant> toVariantMap(Map<String, Object> sToObjectMap)
	{
		Map<String,Variant> variantMap = new HashMap<String, Variant>(sToObjectMap.size());
		for (String key: sToObjectMap.keySet()) {
			if (AboutKeys.ABOUT_SUPPORTED_LANGUAGES.equalsIgnoreCase(key))
			{
				Set<String> value = (Set<String>) sToObjectMap.get(key);
				String[] toArray = value.toArray(new String[]{});
				variantMap.put(key, new Variant(toArray));
			}
			else
			{
				Object value = sToObjectMap.get(key);
				if (value instanceof UUID)
				{
					byte [] byteArr = uuidToByteArray((UUID)value);
					variantMap.put(key, new Variant(byteArr, "ay"));
				}
				else
					variantMap.put(key, new Variant(value));
			}
		}
			
		return variantMap;
	}

	/**
	 * Utility method to convert from a String-Variant map to a String->Object map
	 * @param sToVariantMap input String->Variant map.
	 * @return String->Object map
	 * @throws BusException 
	 */
	public static Map<String, Object> fromVariantMap(Map<String, Variant> sToVariantMap) throws BusException
	{
		Map<String,Object> objectMap = new HashMap<String, Object>(sToVariantMap.size());
		for (String key: sToVariantMap.keySet()) {
			Variant variant = sToVariantMap.get(key);
			Object object = null;
			if (AboutKeys.ABOUT_SUPPORTED_LANGUAGES.equalsIgnoreCase(key))
			{
				String[] languages = variant.getObject(new String[]{}.getClass());
				object = languages;
			}
			else if(AboutKeys.ABOUT_APP_ID.equalsIgnoreCase(key)){
				byte[] b = variant.getObject(new byte[]{}.getClass());
				UUID appId = byteArrayToUUID(b);
				object = appId;
			}
			else
			{
				object = variant.getObject(Object.class);
			}
			objectMap.put(key, object);
		}
	
		return objectMap;
	}

	/**
	 * @param key 
	 * @return
	 */
	private static Class<?> getObjectClass(String key)
	{
		if (AboutKeys.ABOUT_SUPPORTED_LANGUAGES.equalsIgnoreCase(key))
		{
			return new String[]{}.getClass();
		}
		return Object.class;
	}

	public static byte[] toByteArray(char[] charArray)
	{
		byte[] result = new byte[charArray.length];
		for (int i=0; i<charArray.length;i++)
		{
			result[i] = (byte) charArray[i];
		}
		return result;
	}

	public static char[] toCharArray(byte[] byteArray)
	{
		char[] result = new char[byteArray.length];
		for (int i=0; i<byteArray.length;i++)
		{
			result[i] = (char) byteArray[i];
		}
		return result;
	}
	
	public static byte[] uuidToByteArray(UUID uuid) 
	{

    	long msUuid = uuid.getMostSignificantBits();
    	long lsUuid = uuid.getLeastSignificantBits();
    	byte[] byteArrayUuid = new byte[16];

    	for (int i = 0; i < 8; i++) {
    		byteArrayUuid[i] = (byte) (msUuid >>> 8 * (7 - i));
    	}
    	for (int i = 8; i < 16; i++) {
    		byteArrayUuid[i] = (byte) (lsUuid >>> 8 * (7 - i));
    	}

    	return byteArrayUuid;
    }
	
	public static UUID byteArrayToUUID(byte[] bAppId) 
	{
    	long msUuid = 0;
    	long lsUuid = 0;
    	for (int i = 0; i < 8; i++)
    		msUuid = (msUuid << 8) | (bAppId[i] & 0xff);
    	for (int i = 8; i < 16; i++)
    		lsUuid = (lsUuid << 8) | (bAppId[i] & 0xff);
    	UUID result = new UUID(msUuid, lsUuid);

    	return result;
    }
	
}
