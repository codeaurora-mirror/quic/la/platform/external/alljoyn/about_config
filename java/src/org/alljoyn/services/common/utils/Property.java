/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Property
{
	public final static String NO_LANGUAGE = "";
	private final Map<String, Object> m_values = new HashMap<String,Object>(3);
	private final boolean m_isWritable;
	private final boolean m_isAnnounced;
	private final boolean m_isPublic;
	private final String m_name;
	
	public Property(String m_name, boolean isWritable,
			boolean isAnnounced, boolean isPublic)
	{
		super();
		this.m_isWritable = isWritable;
		this.m_isAnnounced = isAnnounced;
		this.m_isPublic = isPublic;
		this.m_name = m_name;
	}
	public boolean isWritable()
	{
		return m_isWritable;
	}
	public boolean isAnnounced()
	{
		return m_isAnnounced;
	}
	public boolean isPublic()
	{
		return m_isPublic;
	}
	public String getName()
	{
		return m_name;
	}
	public void setValue(String language, Object value)
	{
		if (language == null)
		{
			language = NO_LANGUAGE;
		}
		m_values.put(language, value);
	}
	public Object getValue(String language, String defaultLanguage) 
	{
		if (m_values.containsKey(language))
		{
			return m_values.get(language);
		}
		else if (m_values.containsKey(defaultLanguage))
		{
			return m_values.get(defaultLanguage);
		}
		else
		{
			return m_values.get(NO_LANGUAGE);
		}
	}
	public Set<String> getLanguages()
	{
		return m_values.keySet();
	}
	public void remove(String language)
	{
		m_values.remove(language);
	}
	
}
