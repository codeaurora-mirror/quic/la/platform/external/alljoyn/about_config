/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common;

import org.alljoyn.bus.annotation.Position;

/**
 * Descriptions of the BusObjects that make up the service.
 * @author dzehavi
 *
 */
public class BusObjectDescription {
	
	/* The object path of the BusObject */
    @Position(0) public String path;
    /* An array of strings where each element of the array names an AllJoyn interface that is implemented by the given BusObject. */
    @Position(1) public String[] interfaces;
	
	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public String[] getInterfaces()
	{
		return interfaces;
	}

	public void setInterfaces(String[] interfaces)
	{
		this.interfaces = interfaces;
	}
	@Override
	public String toString() {
		
		String s = "";
		s += "path: "+path+".\n";
		s += "interfaces: ";
		for (int i = 0; i < interfaces.length; i++){
			s += interfaces[i];
			if(i != interfaces.length-1)
				s += ",";
			else
				s += ".\n";
		}
		return s;
	}
    
    
}