/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common.utils;


import android.util.Log;

public class AndroidLogger implements GenericLogger {

	/**
	 * @see org.alljoyn.services.utils.iskoot.ioe.ns.commons.GenericLogger#debug(java.lang.String, java.lang.String)
	 */
	@Override
	public void debug(String TAG, String msg) {
		Log.d(TAG, msg);
	}

	/**
	 * @see org.alljoyn.services.utils.iskoot.ioe.ns.commons.GenericLogger#info(java.lang.String, java.lang.String)
	 */
	@Override
	public void info(String TAG, String msg) {
		Log.i(TAG, msg);
	}

	/**
	 * @see org.alljoyn.services.utils.iskoot.ioe.ns.commons.GenericLogger#warn(java.lang.String, java.lang.String)
	 */
	@Override
	public void warn(String TAG, String msg) {
		Log.w(TAG, msg);
	}

	/**
	 * @see org.alljoyn.services.utils.iskoot.ioe.ns.commons.GenericLogger#error(java.lang.String, java.lang.String)
	 */
	@Override
	public void error(String TAG, String msg) {
		Log.e(TAG, msg);
	}

	/**
	 * @see org.alljoyn.services.utils.iskoot.ioe.ns.commons.GenericLogger#fatal(java.lang.String, java.lang.String)
	 */
	@Override
	public void fatal(String TAG, String msg) {
		Log.wtf(TAG, msg);
	}
}
