/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common.storage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.alljoyn.about.AboutKeys;
import org.alljoyn.services.common.utils.Property;
import org.alljoyn.services.common.utils.PropertyParser;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

/**
 * A default implementation of the AboutConfigContainer.
 * Reads the factory defaults from assets/ajaboutconfig_en.txt, assets/ajaboutconfig_fr.txt etc.
 * Stores custom configuration in Shared Preferences.
 *
 */
public class PropertyStoreImpl implements PropertyStore// AboutStore 
{
	private static final String CONFIG_XML = "Config.xml";
	public static final String TAG = PropertyStoreImpl.class.getName();
	private String m_defaultLanguage = "en";
	private Set<String> m_supportedLanguages = new HashSet<String>();
	private String m_passphrase = "000000";

	/** 
	 * configuration storage. 
	 */
	private AssetManager m_assetMgr;
	private Context m_context;
	private Map<String, Property> m_aboutConfigMap;


	public PropertyStoreImpl(Context context)
	{
		super();
		this.m_context = context;
		m_assetMgr = context.getAssets();
		m_context = context;

		// read factory defaults from assets/Config.xml
		loadFactoryDefaults();

		// figure out the available languages from the contents of Config.xml
		loadLanguages();

		// read modified-and-persisted configuration and override factory defaults
		loadStoredConfiguration();

		// read the user's default language
		setDefaultLanguageFromProperties();

		// load the unique Id. Create if not exist
		loadAppId();

		// load the device Id. Create if not exist
		loadDeviceId();

		// load the AJ passphrase
		loadPassphrase();

		storeConfiguration();
	}

	@Override
	public void updateConfiguration(Map<String, Object> newConfiguration, String languageTag) throws IllegalAccessException
	{
		// save in cache
		for (String key: newConfiguration.keySet()) 
		{
			Property Property = m_aboutConfigMap.get(key);
			if (Property == null)
			{
				throw new IllegalAccessException("Unknown property: " + key);
			}
			if (!Property.isWritable())
			{
				throw new IllegalAccessException("Trying to change a read only property: " + key);
			}
			Set<String> langs = Property.getLanguages();
			//In case the field has only one language and it equals "", there
			//will be no possibility to set another value with a different language but "". 
			if(langs != null  && langs.size() == 1){
				Iterator<String> iterator = langs.iterator();
				if(iterator.hasNext()){
					String s = iterator.next();
					if(s.equals(""))
						languageTag = "";
				}
			}			
			Property.setValue(languageTag, newConfiguration.get(key));
		}

		setDefaultLanguageFromProperties();
		// save config map to persistent storage
		storeConfiguration();
	}

	@Override
	public void resetConfiguration(String languageTag, String[] removedFields)
	{
		for (String field: removedFields)
		{
			Property Property = m_aboutConfigMap.get(field);
			if (Property != null)
			{
				Set<String> langs = Property.getLanguages();
				if(langs.size() == 1){
					m_aboutConfigMap.remove(field);
				}
				else{	
					Property.remove(languageTag);
				}
			}
		}

		// save config map to persistent storage
		storeConfiguration();
		loadFactoryDefaults();
		loadStoredConfiguration();
	}

	@Override
	public void setPassphrase(String passphrase)
	{
		m_passphrase = passphrase;
		Property Property = m_aboutConfigMap.get(AboutKeys.ABOUT_PASSPHRASE);
		Property.setValue(Property.NO_LANGUAGE, passphrase);
		storeConfiguration();
	}

	@Override
	public String getPassphrase()
	{
		return m_passphrase;
	}


	@Override
	public void resetToFactoryDefault()
	{
		// delete cache
		m_aboutConfigMap.clear();
		// delete persistent storage
		m_context.deleteFile(CONFIG_XML);
		// load factory defaults
		loadFactoryDefaults();
		// TODO restart as a soft AP...
	}


	public Map<String, Object>  ReadAll(String languageTag, Filter filter)
	{
		switch(filter)
		{
		case ANNOUNCE:
			return getAnnouncement(languageTag);
		case READ:
			return getAbout(languageTag);
		case WRITE:
			return getConfiguration(languageTag);
		default:
			return new HashMap<String, Object>(20);
		}
	}

	private Map<String, Object> getConfiguration(String languageTag) 
	{
		Map<String, Object> configuration = new HashMap<String, Object>(20);
		for (String key:m_aboutConfigMap.keySet())
		{
			Property Property = m_aboutConfigMap.get(key);
			if (!Property.isPublic() 
					|| !Property.isWritable())
			{
				continue;
			}
			Object value = Property.getValue(languageTag, m_defaultLanguage);
			if (value != null)
				configuration.put(key, value);
		}
		return configuration;
	}

	private Map<String, Object> getAbout(String languageTag)
	{
		Map<String, Object> about = new HashMap<String, Object>(20);
		for (String key:m_aboutConfigMap.keySet())
		{
			Property Property = m_aboutConfigMap.get(key);
			if (!Property.isPublic())
			{
				continue;
			}
			Object value = Property.getValue(languageTag, m_defaultLanguage);
			if (value != null)
				about.put(key, value);
		}
		return about;
	} 

	private Map<String, Object> getAnnouncement(String languageTag)
	{
		Map<String, Object> about = new HashMap<String, Object>(20);
		for (String key:m_aboutConfigMap.keySet())
		{
			Property Property = m_aboutConfigMap.get(key);
			if (!Property.isPublic() || !Property.isAnnounced())
			{
				continue;
			}
			Object value = Property.getValue(languageTag, m_defaultLanguage);
			if (value != null)
				about.put(key, value);
		}
		return about;
	}


	public void setValue(String key, Object value, String languageTag)
	{
		Property property = m_aboutConfigMap.get(key);
		if (property == null)
		{
			property = new Property(key, true, true, true);
			m_aboutConfigMap.put(key, property);
		}
		property.setValue(languageTag, value);

	}
	
	private void loadLanguages()
	{
		Set<String> languages = new HashSet<String>(3);
		for (String key: m_aboutConfigMap.keySet())
		{
			Property property = m_aboutConfigMap.get(key);
			Set<String> langs = property.getLanguages();
			if(langs.size() != 0)
				languages.addAll(langs);
		}
		languages.remove(Property.NO_LANGUAGE);
		m_supportedLanguages = languages;
		Property property = new Property(AboutKeys.ABOUT_SUPPORTED_LANGUAGES, false, false, true);
		property.setValue(m_defaultLanguage, m_supportedLanguages);
		m_aboutConfigMap.put(AboutKeys.ABOUT_SUPPORTED_LANGUAGES, property); 
	}

	private void loadPassphrase()
	{
		Property passphraseProperty = m_aboutConfigMap.get(AboutKeys.ABOUT_PASSPHRASE);
		if (passphraseProperty != null)
		{
			m_passphrase = (String) passphraseProperty.getValue(Property.NO_LANGUAGE, Property.NO_LANGUAGE);
		}
	}

	private void loadFactoryDefaults() 
	{
		try
		{
			InputStream is = m_assetMgr.open(CONFIG_XML);
			m_aboutConfigMap = PropertyParser.readFromXML(is);
		} catch (IOException e)
		{
			Log.e(TAG, "Error loading file assets/"+CONFIG_XML, e);
			m_aboutConfigMap = createCannedMap();
		} catch (ParserConfigurationException e)
		{
			Log.e(TAG, "Error parsing xml file assets/"+CONFIG_XML, e);
			m_aboutConfigMap = createCannedMap();
		} catch (SAXException e)
		{
			Log.e(TAG, "Error parsing xml file assets/"+CONFIG_XML, e);
			m_aboutConfigMap = createCannedMap();
		}
	}

	private void loadStoredConfiguration() 
	{
		try
		{
			if (new File(m_context.getFilesDir() + "/" + CONFIG_XML).exists())
			{
				InputStream is = m_context.openFileInput(CONFIG_XML);
				Map<String, Property> storedConfiguration = PropertyParser.readFromXML(is);
				for (String key: storedConfiguration.keySet())
				{
					Property property = m_aboutConfigMap.get(key);
					Property storedProperty = storedConfiguration.get(key);
					if (property == null && storedProperty != null)//should never happen
					{
						m_aboutConfigMap.put(key, storedProperty);
					}
					else
					{
						for (String language: storedProperty.getLanguages())
						{
							Object languageValue = storedProperty.getValue(language, language);
							property.setValue(language, languageValue);
						}
					}
				}
			}
		} catch (IOException e)
		{
			Log.e(TAG, "Error loading file "+CONFIG_XML, e);
		} catch (ParserConfigurationException e)
		{
			Log.e(TAG, "Error parsing xml file "+CONFIG_XML, e);
		} catch (SAXException e)
		{
			Log.e(TAG, "Error parsing xml file "+CONFIG_XML, e);
		}
	}


	private void storeConfiguration() 
	{
		String localConfigFileName = CONFIG_XML; 
		// Note: this one is on the app's folder, not in assets
		try
		{
			FileOutputStream openFileOutput = m_context.openFileOutput(localConfigFileName, Context.MODE_PRIVATE);
			PropertyParser.writeToXML(openFileOutput, m_aboutConfigMap);
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Map<String, Property> createCannedMap()
	{
		Map<String, Property> aboutMap = new HashMap<String, Property>(10);

		Property property = new Property(AboutKeys.ABOUT_APP_NAME, false, true, true);
		property.setValue(m_defaultLanguage, "Demo app by iSkoot");
		aboutMap.put(AboutKeys.ABOUT_APP_NAME, property); 

		property = new Property(AboutKeys.ABOUT_DEVICE_NAME, true, true, true);
		property.setValue(m_defaultLanguage, "Demo app by iSkoot");
		aboutMap.put(AboutKeys.ABOUT_DEVICE_NAME, property); 

		property = new Property(AboutKeys.ABOUT_MANUFACTURER, false, true, true);
		property.setValue(m_defaultLanguage, "Qualcomm Inc.");
		aboutMap.put(AboutKeys.ABOUT_MANUFACTURER, property); 

		property = new Property(AboutKeys.ABOUT_DESCRIPTION, false, false, true);
		property.setValue(m_defaultLanguage, "A default app that demonstrates the About/Config feature");
		aboutMap.put(AboutKeys.ABOUT_DESCRIPTION, property); 

		property = new Property(AboutKeys.ABOUT_DEFAULT_LANGUAGE, true, true, true);
		property.setValue(m_defaultLanguage, m_defaultLanguage);
		aboutMap.put(AboutKeys.ABOUT_DEFAULT_LANGUAGE, property); 

		property = new Property(AboutKeys.ABOUT_SOFTWARE_VERSION, false, false, true);
		property.setValue(m_defaultLanguage, "1.0.0.0");
		aboutMap.put(AboutKeys.ABOUT_SOFTWARE_VERSION, property); 

		property = new Property(AboutKeys.ABOUT_AJ_SOFTWARE_VERSION, false, false, true);
		property.setValue(m_defaultLanguage, "1.0.0.0");
		aboutMap.put(AboutKeys.ABOUT_AJ_SOFTWARE_VERSION, property); 

		property = new Property(AboutKeys.ABOUT_MODEL_NUMBER, false, true, true);
		property.setValue(m_defaultLanguage, "S100");
		aboutMap.put(AboutKeys.ABOUT_MODEL_NUMBER, property); 

		property = new Property(AboutKeys.ABOUT_OEM_REVERSED_DOMAIN_NAME, false, false, true);
		property.setValue(m_defaultLanguage, "com.iskoot");
		aboutMap.put(AboutKeys.ABOUT_OEM_REVERSED_DOMAIN_NAME, property); 

		property = new Property(AboutKeys.ABOUT_SUPPORTED_LANGUAGES, false, false, true);
		property.setValue(m_defaultLanguage, m_supportedLanguages);
		aboutMap.put(AboutKeys.ABOUT_SUPPORTED_LANGUAGES, property); 

		return aboutMap;
	}

	/**
	 * 
	 */
	private void setDefaultLanguageFromProperties()
	{
		Property defaultLanguageProperty = m_aboutConfigMap.get(AboutKeys.ABOUT_DEFAULT_LANGUAGE);
		if (defaultLanguageProperty != null)
		{
			m_defaultLanguage = (String) defaultLanguageProperty.getValue(Property.NO_LANGUAGE, Property.NO_LANGUAGE);
		}
	}

	/**
	 * If appId was not found in factory defaults or on persistent storage, generate it 
	 */
	public void loadAppId()
	{
		Property appIdProperty = m_aboutConfigMap.get(AboutKeys.ABOUT_APP_ID);

		if (appIdProperty == null || appIdProperty.getValue(Property.NO_LANGUAGE, Property.NO_LANGUAGE) == null)
		{
			UUID defaultAppId = UUID.randomUUID();
			//				String sAppId = String.valueOf(TransportUtil.uuidToByteArray(defaultAppId));
			// here we take the stored about map, and fill gaps by default values. We don't shrink the map - other existing values will remain.
			Property property = new Property(AboutKeys.ABOUT_APP_ID, false, true, true);
			property.setValue(Property.NO_LANGUAGE, defaultAppId);
			m_aboutConfigMap.put(AboutKeys.ABOUT_APP_ID, property);
		}
	}

	/**
	 * If uniqueId was not found in factory defaults or on persistent storage, generate it 
	 */
	public void loadDeviceId()
	{
		Property deviceIdProperty = m_aboutConfigMap.get(AboutKeys.ABOUT_DEVICE_ID);

		if (deviceIdProperty == null || deviceIdProperty.getValue(Property.NO_LANGUAGE, Property.NO_LANGUAGE) == null)
		{
			String defaultDeviceId = String.valueOf("IoE"+System.currentTimeMillis());

			// here we take the stored about map, and fill gaps by default values. We don't shrink the map - other existing values will remain.
			Property property = new Property(AboutKeys.ABOUT_DEVICE_ID, false, true, true);
			property.setValue(Property.NO_LANGUAGE, defaultDeviceId);
			m_aboutConfigMap.put(AboutKeys.ABOUT_DEVICE_ID, property);
		}

	}

}
