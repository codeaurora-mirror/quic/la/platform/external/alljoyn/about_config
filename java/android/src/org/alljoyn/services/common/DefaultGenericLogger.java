/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common;

import org.alljoyn.services.common.utils.GenericLogger;

public class DefaultGenericLogger implements GenericLogger {

	@Override
	public void warn(String TAG, String msg)
	{
		System.out.println(new StringBuffer(TAG).append("Warning: ").append(msg).toString());
	}
	
	@Override
	public void info(String TAG, String msg)
	{
		System.out.println(new StringBuffer(TAG).append("Info: ").append(msg).toString());
	}
	
	@Override
	public void fatal(String TAG, String msg)
	{
		System.out.println(new StringBuffer(TAG).append("WTF: ").append(msg).toString());
	}
	
	@Override
	public void error(String TAG, String msg)
	{
		System.out.println(new StringBuffer(TAG).append("Error: ").append(msg).toString());
	}
	
	@Override
	public void debug(String TAG, String msg)
	{
		System.out.println(new StringBuffer(TAG).append("Debug: ").append(msg).toString());
	}
}
